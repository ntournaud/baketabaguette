class ApplicationMailer < ActionMailer::Base
  default from: 'contact@baketabaguette.com'
  layout 'mailer'
end
