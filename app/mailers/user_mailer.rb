class UserMailer < ActionMailer::Base
  default from: "baketabaguette@gmail.com"
 
  def subscribe_confirmation(email)
    mail(to: email, subject: "Your subscription to BakeTaBaguette news has been taken into account! Merci!")
  end

  def subscribe(email)
    @email = email
    mail to: "baketabaguette@gmail.com", subject: "Un nouvel abonnement à la newsletter"
  end

  def booking_confirmation(email)
    mail(to: email, subject: "Votre commande a bien été effectuée !")
  end

  def booking(email)
    @email = email
    mail to: "baketabaguette@gmail.com", subject: "Une nouvelle commande"
  end


  # private
  #   def apply_params
  #     params.require(:user_mailer).permit(:email)
  #   end

end
