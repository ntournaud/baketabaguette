class ChargesController < ApplicationController
	def new
	end

	def create
    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :address => params[:order_address],
      :card  => params[:stripeToken]
    )

    charge = Stripe::Charge.create(
      customer:     customer.id,
      amount:       params[:amount],
      description:  'Bake Ta Baguette',
      currency:     'eur'
    )

    UserMailer.booking_confirmation(params[:stripeEmail]).deliver

    redirect_to thanks_path

    rescue Stripe::CardError => e
      flash[:error] = e.message
      redirect_to charges_path
  end

  def thanks
  end
  
end

