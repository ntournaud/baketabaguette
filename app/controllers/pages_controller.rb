class PagesController < ApplicationController

	def home
	end

	def kits
	end

	def history
	end

	def faq
	end

	def recipe
	end

	def order
	end

	def workshop
	end

	def workshopenterprise
	end

	def about
	end

	def ingredients
	end

	def subscribed
    UserMailer.subscribe_confirmation(params[:email]).deliver
    UserMailer.subscribe(params[:email]).deliver
    redirect_to :action => "home"
	end
end
