class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  before_filter do
    if request.ssl? && Rails.env.production?
      redirect_to "http://#{request.host_with_port}#{request.fullpath}", :status => :moved_permanently
    end
  end

  before_action :set_locale
   # To set different languages for the site
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options(options={})
    options = options.merge({ locale: I18n.locale })
    options = options.merge({ subdomain: 'www' }) if Rails.env.heroku_prod?
    options
  end
end
