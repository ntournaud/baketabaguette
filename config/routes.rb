Rails.application.routes.draw do
  mount ForestLiana::Engine => '/forest'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope '/(:locale)', constraints: {locale: /en|fr/} do
    root 'pages#home'
    get 'kits' => 'pages#kits', as: :kits
    get 'ingredients' => 'pages#ingredients', as: :ingredients
    get 'history' => 'pages#history', as: :history
    get 'faq' => 'pages#faq', as: :faq
    get 'recipe' => 'pages#recipe', as: :recipe
  	get 'workshop' => 'pages#workshop', as: :workshop
    get 'workshop-entreprise' => 'pages#workshopentreprise', as: :workshopentreprise
  	get 'enterprise' => 'pages#enterprise', as: :enterprise
  	get 'order' => 'pages#order', as: :order
    get 'subscribed' => 'pages#subscribed', as: :subscribed
    get 'about' => 'pages#about', as: :about
    resources :charges
    get 'thanks', to: 'charges#thanks', as: 'thanks'
  end
  get '/:locale' => 'pages#home'
end
